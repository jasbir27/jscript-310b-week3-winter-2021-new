// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price


const coke13 = { descr: 'Coke', price: 1.99 };
const pizza2 = { descr: 'Pizza', price: 5.99 };

const logReceipt4 = (...groceries) => {
    let total2 = 0;
    groceries.forEach(grocery => {
        console.log(`${grocery.descr} - $${grocery.price}`);
        total2 += grocery.price;
    });
    console.log("Total - $" + total2);
};

logReceipt(coke13, pizza2);



// Check
logReceipt(
  { descr: 'Burrito', price: 5.99 },
  { descr: 'Chips & Salsa', price: 2.99 },
  { descr: 'Sprite', price: 1.99 }
);
// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
// Total - $10.97
